import path from 'path'
import React from 'react'

const contentful = require('contentful')

const space = '1h01r2li312k'
const accessToken = '95522cc780d20fded0308fef52bffae04fc39fff388dbf2d9c2b4fedf1438298'
const environment = 'master'

const client = contentful.createClient({
  space,
  accessToken,
  environment
})

// https://github.com/react-static/react-static/blob/master/docs/config.md/#webpack

export default {
  getSiteData: async ({ dev }) => ({
    title: 'HeadBox Marketplace Content',
    lastBuilt: Date.now()
  }),
  Document: ({ Html, Head, Body, children, state: { siteData, renderMeta } }) => (
    <Html lang='en-US'>
      <Head>
        <meta charSet='UTF-8' />
        <title>HeadBox Marketplace Static Site</title>
        <meta name='viewport' content='width=device-width, initial-scale=1' />
        <meta name='description' content='HeadBox Marketplace Static Site' />
      </Head>
      <Body>
        <div id='root'>{children}</div>
      </Body>
    </Html>
  ),
  getRoutes: async () => {
    let data = {}

    client
      // .getEntries()
      .getEntry('2Hw9MqwmKQw86sym2YcsIg')
      .then(response => {
        data = response
        console.log('==== res', response)
      })
      .catch(console.error)

    return [
      {
        path: '/',
        template: 'src/pages/Landing.js',
        getData: () => ({ page: data })
        // children: posts.map(post => ({
        //   path: `/post/${post.id}`,
        //   // template: 'src/containers/Post',
        //   getData: () => ({
        //     post
        //   })
        // }))
      }
    ]
  },
  plugins: [
    [
      require.resolve('react-static-plugin-source-filesystem'),
      {
        location: path.resolve('./src/pages')
      }
    ],
    require.resolve('react-static-plugin-sitemap'),
    require.resolve('react-static-plugin-emotion')
  ]
}
