#!/bin/sh

echo 'Beginning deployment ! ✨'

echo 'Inspect bundlesize 👀'

bundlesize

# echo 'building new image...'

# react-static build --debug

echo 'Netlify deploy to production...'

netlify deploy --prod --open 

echo 'Success !!! 🥂