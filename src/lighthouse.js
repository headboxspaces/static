// const lighthouse = require('lighthouse')
// const chromeLauncher = require('chrome-launcher')

const appUrl = 'http://localhost:3000'

const flagsOBJ = {
  chromeFlags: ['--headless']
}
export const launchChromeAndRunLighthouse = (url = appUrl, flags = flagsOBJ, config = null) => {
  return chromeLauncher.launch(flags).then(chrome => {
    flags.port = chrome.port
    return lighthouse(url, flags, config).then(results => chrome.kill().then(() => results))
  })
}

// // Usage:
// launchChromeAndRunLighthouse(appUrl, opts).then(results => {
//   console.log('results are-----', results)
// })

// const log = require('lighthouse-logger')

// const flags = { logLevel: 'info' }
// log.setLevel(flags.logLevel)

// launchChromeAndRunLighthouse('https://example.com', flags).then(...);
