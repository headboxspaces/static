import React, { useState, useEffect } from 'react'
import { jsx, css } from '@emotion/core'
import styled from '@emotion/styled'

// abstracting styled component
export const StyledButton = styled.button`
  padding: 10px;
  margin: 5px;
  font-size: 14px;
  background-color: transparent;
  border: 1px solid white;
  border-radius: 3px;
  color: white;
  &:hover {
    color: lightgrey;
    border: 1px solid lightgrey;
  }
`

export const STYLE = {
  COVER: { height: 500, objectFit: 'cover', width: '100%' },
  CONTAINER: {
    display: 'flex',
    minWidth: 800,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    minHeigh: '200vh',
    position: 'relative',
    textAlign: 'center'
  },
  BANNER: {
    height: 70,
    minWidth: 500,
    backgroundColor: 'darkcyan',
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  FONT: {
    position: 'absolute',
    textShadow:
      '0px 4px 3px rgba(0, 0, 0, 0.5),0px 8px 13px rgba(0, 0, 0, 0.5),0px 18px 23px rgba(0, 0, 0, 0.5)',
    left: '5%',
    color: 'white',
    top: 40
  },
  PARAGRAPH: {
    display: 'flex',
    flexDirection: 'column',
    justifyItems: 'center',
    alignItems: 'center',
    padding: 30,
    minWidth: 500
  },
  CARD: {
    position: 'absolute',
    right: '5%',
    top: 160,
    height: 300,
    width: 400,
    borderRadius: 2,
    backgroundColor: 'white',
    opacity: 0.94,
    boxShadow: '3px 3px 10px rgba(0, 0, 0, 0.4)'
  }
}
