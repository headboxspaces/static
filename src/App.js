import React, { Suspense } from 'react'
import { Root, Routes, addPrefetchExcludes } from 'react-static'
import { Landing } from './pages'
import { Router, Switch, Route, Link } from 'react-router-dom'
import { createMemoryHistory } from 'history'
const history = createMemoryHistory()
import './app.css'
const NAV_STYLE = { backgroundColor: 'white', boxShadow: '0 8px 8px -8px lightgrey' }
// Any routes that start with 'dynamic' will be treated as non-static routes
addPrefetchExcludes(['dynamic'])

const App = () => {
  return (
    <>
      <Router history={history}>
        <nav style={NAV_STYLE} />

        <div className='content'>
          <Suspense fallback={<em>Loading...</em>}>
            <Switch>
              <Route component={Landing} exact path='/' />
            </Switch>
          </Suspense>
        </div>
      </Router>
    </>
  )
}

export default App
