import React from 'react'
import { StyledButton, STYLE } from '../style'
import { withRouteData, useRouteData } from 'react-static'

const Landing = () => {
  const { page } = useRouteData()

  if (page.fields) {
    const { backgroundImage, strapline, subheading, eventBanner, location } = page.fields
    const optimizedUrl = url =>
      `${url.replace(
        '//images.ctfassets.net/1h01r2li312k',
        '//headbox-cf.imgix.net'
      )}?q=25&bri=15&sharp=30`

    return (
      <>
        <header style={{ height: 80 }} />
        <main>
          <div style={STYLE.CONTAINER}>
            <img
              src={optimizedUrl(backgroundImage.fields.file.url)}
              alt={strapline + ' ' + subheading}
              style={STYLE.COVER}
            />
            <h1 style={STYLE.FONT}>{strapline}</h1>
            <h3 style={{ ...STYLE.FONT, top: 90, marginTop: 10 }}>{subheading}</h3>
            <section style={STYLE.CARD}>
              <h2 style={{ marginTop: 40, fontWeight: 'bold' }}>{eventBanner.fields.heroTitle}</h2>
            </section>
          </div>
          <section style={STYLE.BANNER}>
            <h4 style={{ color: 'white' }}>Looking to list your venue on HeadBox?</h4>
            <StyledButton>find out more</StyledButton>
          </section>
          <br />
          <section style={STYLE.PARAGRAPH}>
            <h3>HeadBox accross Europe</h3>
            <br />
            <h4 style={{ textAlign: 'center' }}>
              Connecting businesses across the UK, Ireland and the Netherlands with the perfect
              venue for their meeting, offsite or event
            </h4>
          </section>
          <div style={{ margin: 20, width: 480 }}>
            <img
              src={optimizedUrl(location.fields.tileImage.fields.file.url)}
              alt={location.fields.name}
              style={{ objectFit: 'cover', height: 200, width: 480 }}
            />
            <h4 style={{ textAlign: 'center', margin: 10 }}>{location.fields.name}</h4>
            <br />
          </div>
        </main>
        <footer style={{ height: 300, backgroundColor: 'whitesmoke' }} />
      </>
    )
  } else return <main style={STYLE.CONTAINER}>Loading....</main>
}

export default Landing
