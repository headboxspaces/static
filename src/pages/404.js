import React from 'react'

const PageNotFound = () => (
  <div>
    <h1>404 - Oh no's! We couldn't find that page :(</h1>
  </div>
)

export default PageNotFound
