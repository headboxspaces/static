import React from 'react'
import ReactDOM from 'react-dom'
import { AppContainer } from 'react-hot-loader'
import { createBrowserHistory } from 'history'
import { BrowserRouter } from 'react-router-dom'
// Your top level component
import App from './App'

// Export your top level component as JSX (for static rendering)
export default App
let history
// Render your app
if (typeof document !== 'undefined') {
  // const createBrowserHistory = require('history').createBrowserHistory

  // history = createBrowserHistory()
  const target = document.getElementById('root')

  const renderMethod = target.hasChildNodes() ? ReactDOM.hydrate : ReactDOM.render

  const render = Comp => {
    renderMethod(
      <BrowserRouter>
        <AppContainer>
          <Comp />
        </AppContainer>
      </BrowserRouter>,
      target
    )
  }

  // Render!
  render(App)

  // Hot Module Replacement
  if (module && module.hot) {
    module.hot.accept('./App', () => {
      render(App)
    })
  }
}
