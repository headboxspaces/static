# HeadBox Marketplace Static Site

To use this template, run `yarn` to install dependencies and `yarn start` to run it locally.

## Netlify CLI

[![Netlify Status](https://api.netlify.com/api/v1/badges/838a1b00-87d4-423d-9369-ecd470fa7a16/deploy-status)](https://app.netlify.com/sites/elegant-hypatia-ea62ba/deploys)

More on using [Netlify cli](https://www.netlify.com/blog/2019/05/28/deploy-in-seconds-with-netlify-cli/)

First build bundle (dist):
`react-static build --debug` or just `yarn build`

provided you have corect acces and setup `netlify login`, deploy the site to prod:
`netlify deploy --prod` or just `yarn deploy`

## Performance Adits: Lighthouse CLI and Bundlesize

More on using [Lighthouse cli](https://github.com/GoogleChrome/lighthouse)

More on enforcing [bundlesize limits in CI](https://web.dev/incorporate-performance-budgets-into-your-build-tools/)

To run Performance Audit reports using Lighthouse cli, run:
`lighthouse <url> --view`

for example:
`lighthouse https://elegant-hypatia-ea62ba.netlify.com --view`

to inspect the bundle size run `yarn size`

## Contentful Delivery SDK

to avoid messy GET requests - [Contetful.js](https://contentful.github.io/contentful.js/contentful/7.14.0/) module is used to simplify retrieval of content.

## Styling with Emotion

In the absense of third party UI system - full benefit of styling is achieved using [Emotion.js](https://github.com/emotion-js/emotion) - it allows for abstracting styles as components (styled components) as well as camelCase and CSS styling.

## Optimising Images with ImgIX

to take off any unnecessary bloat off the image sizes, all of the image sources are called via [imgIX API](https://dashboard.imgix.com/sources/5d7b6dfad596f1000171cd30)
